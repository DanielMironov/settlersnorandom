package com.game.settlersnorandom

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.game.settlersnorandom.ui.GameState
import com.game.settlersnorandom.ui.StartGameUI
import com.game.settlersnorandom.model.PlayerColor


const val PLAYER_COLOR_KEY = "PLAYER_COLOR_KEY"

class MainActivity : AppCompatActivity(), StartGameUI {

    override val gameState: GameState = GameStateProvider.provide()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun startGame(view: View) {
        val firstRedAddSettleIntent = Intent(this, AddSettlementActivity::class.java)
            .putExtra(PLAYER_COLOR_KEY, PlayerColor.RED.name)
        startActivityForResult(firstRedAddSettleIntent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 8) {
            AddedSettlementCollector.startGameState(gameState)
            AddedSettlementCollector.clear()
            val turnIntent = Intent(this, TurnInfoActivity::class.java)
            startActivity(turnIntent)
            finish()
        } else {
            startAddSettlementActivity(getPlayerColorFromCode(requestCode), requestCode + 1)
        }
    }

    private fun getPlayerColorFromCode(requestCode: Int): PlayerColor {
        return when (requestCode) {
            1 -> PlayerColor.RED
            2 -> PlayerColor.BLACK
            3 -> PlayerColor.BLACK
            4 -> PlayerColor.WHITE
            5 -> PlayerColor.WHITE
            6 -> PlayerColor.YELLOW
            7 -> PlayerColor.YELLOW
            8 -> PlayerColor.RED
            else -> throw IllegalArgumentException("Wrong request code!")
        }
    }

    private fun startAddSettlementActivity(playerColor: PlayerColor, requestCode: Int) {
        val addIntent = Intent(this, AddSettlementActivity::class.java)
            .putExtra(PLAYER_COLOR_KEY, playerColor.name)
        startActivityForResult(addIntent, requestCode)
    }
}
