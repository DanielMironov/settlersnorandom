package com.game.settlersnorandom

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import com.game.settlersnorandom.model.PlayerColor
import com.game.settlersnorandom.model.Resource
import com.game.settlersnorandom.ui.GameState
import com.game.settlersnorandom.ui.PlayerGameUI
import com.game.settlersnorandom.view.SettlementView

class PlayerInfoActivity : AppCompatActivity(), PlayerGameUI {

    override val gameState: GameState = GameStateProvider.provide()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_info)
        val colorText = findViewById<TextView>(R.id.playerColorInfoText)
        colorText.text = getColorTextFromPlayerColor(getPlayerColor())


        showState()
    }

    fun onAddSettlement(view: View) {
        val addSettleIntent = Intent(this, AddSettlementActivity::class.java)
            .putExtra(PLAYER_COLOR_KEY, getPlayerColor().name)
        startActivityForResult(addSettleIntent, 15)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 15) {
            AddedSettlementCollector.addSettlementsToGameState(gameState)
            AddedSettlementCollector.clear()

            cleanTable()

            showState()
        }
    }


    private fun cleanTable() {

        val table = findViewById<TableLayout>(R.id.playedSettlementsTable)

        val childCount = table.childCount

        // Remove all rows
        if (childCount > 0) {
            table.removeViews(0, childCount)
        }
    }


    private fun showState() {
        showState(getPlayerColor())
    }

    override fun showState(playerColor: PlayerColor) {
        val playerView = gameState.getPlayersViews()
            .firstOrNull { it.playerColor == playerColor } ?: throw IllegalArgumentException("No such player!")
        val settlements = playerView.settlements

        val settlementsTable = findViewById<TableLayout>(R.id.playedSettlementsTable)

        for (settlement in settlements) {
            settlementsTable.addView(createTableRowFromSettlementView(settlement))
        }
    }

    private fun createTableRowFromSettlementView(settlementView: SettlementView): TableRow {
        val tableRow = TableRow(this)
        val layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
        tableRow.layoutParams = layoutParams

        val isTownCheckBox = CheckBox(this)
        isTownCheckBox.isChecked = settlementView.isTown

        val upgradeButton = Button(this)
        upgradeButton.setOnClickListener {
            gameState.upgradeSettlement(settlementView, getPlayerColor())
            cleanTable()
            showState()
        }
        upgradeButton.text = getString(R.string.upgrade_settlement_button)

        if (settlementView.tiles.size != 3)
            throw IllegalArgumentException("Settlement must have 3 tiles!")

        val firstTile = settlementView.tiles.first()
        val secondTile = settlementView.tiles[1]
        val thirdTile = settlementView.tiles.last()

        val resourceTextView1 = TextView(this)
        resourceTextView1.text = getResourceTextFromResource(firstTile.resource)
        val amountTextView1 = TextView(this)
        amountTextView1.text = firstTile.digit.toString()
        val resourceTextView2 = TextView(this)
        resourceTextView2.text = getResourceTextFromResource(secondTile.resource)
        val amountTextView2 = TextView(this)
        amountTextView2.text = secondTile.digit.toString()
        val resourceTextView3 = TextView(this)
        resourceTextView3.text = getResourceTextFromResource(thirdTile.resource)
        val amountTextView3 = TextView(this)
        amountTextView3.text = thirdTile.digit.toString()

        tableRow.addView(resourceTextView1)
        tableRow.addView(amountTextView1)
        tableRow.addView(resourceTextView2)
        tableRow.addView(amountTextView2)
        tableRow.addView(resourceTextView3)
        tableRow.addView(amountTextView3)

        tableRow.addView(isTownCheckBox)
        if (!settlementView.isTown) {
            tableRow.addView(upgradeButton)
        }

        return tableRow
    }

    private fun getResourceTextFromResource(resource: Resource): String {
        return when (resource) {

            Resource.NONE -> getString(R.string.none_name)
            Resource.CLAY -> getString(R.string.clay_name)
            Resource.WOOD -> getString(R.string.wood_name)
            Resource.STONE -> getString(R.string.stone_name)
            Resource.WHEAT -> getString(R.string.wheat_name)
        }
    }

    private fun getPlayerColor(): PlayerColor {
        return PlayerColor.valueOf(intent.getStringExtra(PLAYER_COLOR_KEY))
    }

    private fun getColorTextFromPlayerColor(playerColor: PlayerColor): String {
        return when (playerColor) {
            PlayerColor.RED -> getString(R.string.red_color)
            PlayerColor.BLACK -> getString(R.string.black_color)
            PlayerColor.WHITE -> getString(R.string.white_color)
            PlayerColor.YELLOW -> getString(R.string.yellow_color)
        }
    }
}
