package com.game.settlersnorandom.listeners

import android.view.View
import android.widget.AdapterView
import android.widget.TextView

class SpinnerWithTextViewListener(private val textView: TextView) : AdapterView.OnItemSelectedListener {

    override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
        val selectedString = parent.getItemAtPosition(position).toString()
        textView.text = selectedString
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        textView.text = textView.text
    }
}