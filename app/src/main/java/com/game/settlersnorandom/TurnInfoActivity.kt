package com.game.settlersnorandom

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import com.game.settlersnorandom.model.PlayerColor
import com.game.settlersnorandom.model.Resource
import com.game.settlersnorandom.ui.GameState
import com.game.settlersnorandom.ui.TurnGameUI

class TurnInfoActivity : AppCompatActivity(), TurnGameUI {

    override val gameState: GameState = GameStateProvider.provide()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_turn_info)

        val redPlayerIncomeTextView = findViewById<TextView>(R.id.redPlayerIncomeText)
        val redIncomeText = getString(R.string.red_color) + " " + getString(R.string.income_message)
        redPlayerIncomeTextView.text = redIncomeText
        val blackPlayerIncomeTextView = findViewById<TextView>(R.id.blackPlayerIncomeText)
        val blackIncomeText = getString(R.string.black_color) + " " + getString(R.string.income_message)
        blackPlayerIncomeTextView.text = blackIncomeText
        val whitePlayerIncomeTextView = findViewById<TextView>(R.id.whitePlayerIncomeText)
        val whiteIncomeText = getString(R.string.white_color) + " " + getString(R.string.income_message)
        whitePlayerIncomeTextView.text = whiteIncomeText
        val yellowPlayerIncomeTextView = findViewById<TextView>(R.id.yellowPlayerIncomeText)
        val yellowIncomeText = getString(R.string.yellow_color) + " " + getString(R.string.income_message)
        yellowPlayerIncomeTextView.text = yellowIncomeText

    }

    fun showRedPlayerButton(view: View) {
        showPlayer(PlayerColor.RED)
    }

    fun showBlackPlayerButton(view: View) {
        showPlayer(PlayerColor.BLACK)
    }

    fun showWhitePlayerButton(view: View) {
        showPlayer(PlayerColor.WHITE)
    }

    fun showYellowPlayerButton (view: View) {
        showPlayer(PlayerColor.YELLOW)
    }

    private fun showPlayer(playerColor: PlayerColor) {
        val showPlayerIntent = Intent(this, PlayerInfoActivity::class.java)
            .putExtra(PLAYER_COLOR_KEY, playerColor.name)
        startActivity(showPlayerIntent)
    }

    fun nextTurnButton(view: View) {
        val redPlayerTable = findViewById<TableLayout>(R.id.redPlayerIncomeTable)
        val blackPlayerTable = findViewById<TableLayout>(R.id.blackPlayerIncomeTable)
        val whitePlayerTable = findViewById<TableLayout>(R.id.whitePlayerIncomeTable)
        val yellowPlayerTable = findViewById<TableLayout>(R.id.yellowPlayerIncomeTable)

        cleanTable(redPlayerTable)
        cleanTable(blackPlayerTable)
        cleanTable(whitePlayerTable)
        cleanTable(yellowPlayerTable)
        nextTurn()
    }

    private fun cleanTable(table: TableLayout) {

        val childCount = table.childCount

        // Remove all rows
        if (childCount > 0) {
            table.removeViews(0, childCount)
        }
    }

    override fun showIncome(incomePerPlayer: Map<PlayerColor, Map<Resource, Int>>) {
        for (player in incomePerPlayer) {
            val tableLayout = getTableByPlayerColor(player.key)
            for (resource in player.value) {
                if ((resource.value > 0) && (resource.key != Resource.NONE)) {
                    val tableRow = createTableRowFromResourceAndAmount(resource.key, resource.value)
                    tableLayout.addView(tableRow)
                }
            }
        }
    }

    private fun getTableByPlayerColor(playerColor: PlayerColor): TableLayout {
        val redPlayerTable = findViewById<TableLayout>(R.id.redPlayerIncomeTable)
        val blackPlayerTable = findViewById<TableLayout>(R.id.blackPlayerIncomeTable)
        val whitePlayerTable = findViewById<TableLayout>(R.id.whitePlayerIncomeTable)
        val yellowPlayerTable = findViewById<TableLayout>(R.id.yellowPlayerIncomeTable)

        return when (playerColor) {

            PlayerColor.RED -> redPlayerTable
            PlayerColor.BLACK -> blackPlayerTable
            PlayerColor.WHITE -> whitePlayerTable
            PlayerColor.YELLOW -> yellowPlayerTable
        }
    }

    private fun createTableRowFromResourceAndAmount(resource: Resource, amount: Int): TableRow {
        val tableRow = TableRow(this)
        val layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT)
        tableRow.layoutParams = layoutParams
        val resourceTextView = TextView(this)
        val amountTextView = TextView(this)
        amountTextView.text = amount.toString()
        resourceTextView.text = getResourceTextFromResource(resource)
        tableRow.addView(resourceTextView)
        tableRow.addView(amountTextView)
        return tableRow
    }

    private fun getResourceTextFromResource(resource: Resource): String {
        return when (resource) {

            Resource.NONE -> getString(R.string.none_name)
            Resource.CLAY -> getString(R.string.clay_name)
            Resource.WOOD -> getString(R.string.wood_name)
            Resource.STONE -> getString(R.string.stone_name)
            Resource.WHEAT -> getString(R.string.wheat_name)
        }
    }
}
