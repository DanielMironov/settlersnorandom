package com.game.settlersnorandom.mappers

import com.game.settlersnorandom.view.SettlementView
import com.game.settlersnorandom.view.TileView
import com.game.settlersnorandom.model.Settlement
import com.game.settlersnorandom.model.Tile
import com.game.settlersnorandom.model.incomeCoefficient
import kotlin.math.abs

fun mapSettlementFromView(settlementView: SettlementView): Settlement {
    return Settlement(settlementView.tiles.map { mapTileFromView(it) })
}

fun mapTileFromView(tileView: TileView): Tile {
    return Tile(tileView.resource, tileView.digit)
}
