package com.game.settlersnorandom.mappers

import com.game.settlersnorandom.model.Player
import com.game.settlersnorandom.model.Settlement
import com.game.settlersnorandom.model.Tile
import com.game.settlersnorandom.model.Town
import com.game.settlersnorandom.view.PlayerView
import com.game.settlersnorandom.view.SettlementView
import com.game.settlersnorandom.view.TileView

/**
 * Created by Daniel Mironov on 05.04.2019
 */

fun mapPlayerToView(player: Player): PlayerView {
    val settlements = player.getSettlementsList().map { mapSettlementToView(it) }
    return PlayerView(settlements, player.playerColor)
}

fun mapSettlementToView(settlement: Settlement): SettlementView {
    val tiles = settlement.tiles.map { mapTileToView(it) }
    val isTown = (settlement is Town)
    return SettlementView(tiles, isTown)
}

fun mapTileToView(tile: Tile): TileView {
    return TileView(tile.digit, tile.calculateResourceAmount(), tile.resource)
}