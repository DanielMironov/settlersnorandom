package com.game.settlersnorandom.view

/**
 * Created by Daniel Mironov on 05.04.2019
 */

data class SettlementView(val tiles: List<TileView>, val isTown: Boolean)