package com.game.settlersnorandom.view

import com.game.settlersnorandom.model.Resource

/**
 * Created by Daniel Mironov on 05.04.2019
 */

data class TileView(val digit: Int, val income: Double, val resource: Resource)