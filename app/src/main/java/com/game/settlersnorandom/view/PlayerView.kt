package com.game.settlersnorandom.view

import com.game.settlersnorandom.model.PlayerColor

/**
 * Created by Daniel Mironov on 05.04.2019
 */

data class PlayerView(val settlements: List<SettlementView>, val playerColor: PlayerColor)