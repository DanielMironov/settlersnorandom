package com.game.settlersnorandom

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import com.game.settlersnorandom.listeners.SpinnerWithTextViewListener
import com.game.settlersnorandom.model.PlayerColor
import com.game.settlersnorandom.model.Resource
import com.game.settlersnorandom.model.Settlement
import com.game.settlersnorandom.model.Tile

class AddSettlementActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_settlement)

        val playerColor: PlayerColor = PlayerColor.valueOf(intent.getStringExtra(PLAYER_COLOR_KEY))

        findViewById<TextView>(R.id.userColorTextAddSettlement)
            .apply { text = getColorTextFromPlayerColor(playerColor) }
            .setTextColor(getColorIdFromPlayerColor(playerColor))

        val firstResourceSpinner = findViewById<Spinner>(R.id.firstResourceSpinner)
        val secondResourceSpinner = findViewById<Spinner>(R.id.secondResourceSpinner)
        val thirdResourceSpinner = findViewById<Spinner>(R.id.thirdResourceSpinner)

        val firstDigitSpinner = findViewById<Spinner>(R.id.firstDigitSpinner)
        val secondDigitSpinner = findViewById<Spinner>(R.id.secondDigitSpinner)
        val thirdDigitSpinner = findViewById<Spinner>(R.id.thirdDigitSpinner)

        prepareSpinner(firstResourceSpinner, R.array.resources_array, R.id.firstTileResourceText)
        prepareSpinner(secondResourceSpinner, R.array.resources_array, R.id.secondTileResourceText)
        prepareSpinner(thirdResourceSpinner, R.array.resources_array, R.id.thirdTileResourceText)

        prepareSpinner(firstDigitSpinner, R.array.digit_array, R.id.firstTileDigitText)
        prepareSpinner(secondDigitSpinner, R.array.digit_array, R.id.secondTileDigitText)
        prepareSpinner(thirdDigitSpinner, R.array.digit_array, R.id.thirdTileDigitText)
    }

    private fun prepareSpinner(spinner: Spinner, textArrayResId: Int, textViewId: Int) {
        ArrayAdapter.createFromResource(
            this,
            textArrayResId,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        val textView = findViewById<TextView>(textViewId)

        spinner.onItemSelectedListener = SpinnerWithTextViewListener(textView)
    }

    private fun getColorIdFromPlayerColor(playerColor: PlayerColor): Int {
        return when (playerColor) {
            PlayerColor.RED -> R.color.redColor
            PlayerColor.BLACK -> R.color.blackColor
            PlayerColor.WHITE -> R.color.whiteColor
            PlayerColor.YELLOW -> R.color.yellowColor
        }
    }

    private fun getColorTextFromPlayerColor(playerColor: PlayerColor): String {
        return when (playerColor) {
            PlayerColor.RED -> getString(R.string.red_color)
            PlayerColor.BLACK -> getString(R.string.black_color)
            PlayerColor.WHITE -> getString(R.string.white_color)
            PlayerColor.YELLOW -> getString(R.string.yellow_color)
        }
    }

    fun onCreateSettlement(view: View) {
        val playerColor: PlayerColor = PlayerColor.valueOf(intent.getStringExtra(PLAYER_COLOR_KEY))

        val firstTileResourceTextView = findViewById<TextView>(R.id.firstTileResourceText)
        val secondTileResourceTextView = findViewById<TextView>(R.id.secondTileResourceText)
        val thirdTileResourceTextView = findViewById<TextView>(R.id.thirdTileResourceText)

        val firstTileDigitTextView = findViewById<TextView>(R.id.firstTileDigitText)
        val secondTileDigitTextView = findViewById<TextView>(R.id.secondTileDigitText)
        val thirdTileDigitTextView = findViewById<TextView>(R.id.thirdTileDigitText)

        val firstTile = getTileFromTextViews(firstTileResourceTextView, firstTileDigitTextView)
        val secondTile = getTileFromTextViews(secondTileResourceTextView, secondTileDigitTextView)
        val thirdTile = getTileFromTextViews(thirdTileResourceTextView, thirdTileDigitTextView)

        val settlement = Settlement(listOf(firstTile, secondTile, thirdTile))
        AddedSettlementCollector.addSettlement(playerColor, settlement)
        finish()
    }

    private fun getTileFromTextViews(resourceTextView: TextView, digitTextView: TextView): Tile {
        return Tile(
            getResourceFromString(
                resourceTextView.text.toString()
            ),
            digitTextView.text.toString().toInt()
        )
    }

    private fun getResourceFromString(string: String): Resource {
        return when (string) {
            getString(R.string.wood_name) -> Resource.WOOD
            getString(R.string.clay_name) -> Resource.CLAY
            getString(R.string.stone_name) -> Resource.STONE
            getString(R.string.wheat_name) -> Resource.WHEAT
            getString(R.string.none_name) -> Resource.NONE
            else -> throw IllegalArgumentException("Wrong string res id!")
        }
    }
}
