package com.game.settlersnorandom

import com.game.settlersnorandom.model.PlayerColor
import com.game.settlersnorandom.model.Settlement
import com.game.settlersnorandom.ui.GameState

object AddedSettlementCollector {
    private val addedSettlements = HashMap<PlayerColor, MutableSet<Settlement>>()

    fun getAddedSettlementsMap(): Map<PlayerColor, MutableSet<Settlement>> {
        return addedSettlements.toMap()
    }

    fun clear() {
        addedSettlements.clear()
    }

    fun addSettlement(playerColor: PlayerColor, settlement: Settlement) {
        val settlementList = addedSettlements[playerColor]
        if (settlementList == null) {
            addedSettlements[playerColor] = mutableSetOf(settlement)
        } else {
            settlementList.add(settlement)
        }
    }

    fun startGameState(gameState: GameState) {
        gameState.start(addedSettlements.toMap())
    }

    fun addSettlementsToGameState(gameState: GameState) {
        for (player in addedSettlements) {
            for (settlement in player.value) {
                gameState.addSettlement(settlement, player.key)
            }
        }
    }
}