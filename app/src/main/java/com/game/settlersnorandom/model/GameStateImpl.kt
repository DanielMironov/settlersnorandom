package com.game.settlersnorandom.model

import com.game.settlersnorandom.view.PlayerView
import com.game.settlersnorandom.view.SettlementView
import com.game.settlersnorandom.mappers.mapPlayerToView
import com.game.settlersnorandom.mappers.mapSettlementFromView
import com.game.settlersnorandom.ui.GameState

/**
 * Created by Daniel Mironov on 22.03.2019
 */

class GameStateImpl : GameState {
    private val players: List<Player> =
        listOf(
            Player(PlayerColor.RED),
            Player(PlayerColor.BLACK),
            Player(PlayerColor.YELLOW),
            Player(PlayerColor.WHITE)
        )

    override fun getPlayersViews(): List<PlayerView> {
        return players.map { mapPlayerToView(it) }
    }

    override fun start(settlementsPerPlayer: Map<PlayerColor, Set<Settlement>>) {
        for (player in players) {
            val currentSettlements = settlementsPerPlayer[player.playerColor]
                ?: throw IllegalArgumentException("Each player must have start settlements!")
            if (currentSettlements.size != 2) {
                throw IllegalArgumentException("Each player must have exactly 2 start settlements!")
            }
            player.start(currentSettlements.first(), currentSettlements.last())
        }
    }

    override fun nextTurn(): Map<PlayerColor, Map<Resource, Int>> {
        val result: MutableMap<PlayerColor, Map<Resource, Int>> = HashMap()
        players.forEach {
            result[it.playerColor] = it.calculateIncome()
        }
        return result.toMap()
    }

    override fun addSettlement(settlement: Settlement, playerColor: PlayerColor) {
        getPlayerByColor(playerColor).addSettlement(settlement)
    }

    override fun upgradeSettlement(settlement: SettlementView, playerColor: PlayerColor) {
        getPlayerByColor(playerColor).upgradeSettlement(mapSettlementFromView(settlement))
    }

    private fun getPlayerByColor(playerColor: PlayerColor): Player {
        return players.firstOrNull { it.playerColor == playerColor }
            ?: throw IllegalArgumentException("No player with that color!")
    }

}