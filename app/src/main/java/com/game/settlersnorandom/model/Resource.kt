package com.game.settlersnorandom.model

/**
 * Created by Daniel Mironov on 22.03.2019
 */

enum class Resource {
    NONE,CLAY,WOOD,STONE,WHEAT
}