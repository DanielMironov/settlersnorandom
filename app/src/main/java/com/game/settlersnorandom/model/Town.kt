package com.game.settlersnorandom.model

/**
 * Created by Daniel Mironov on 25.03.2019
 */

class Town(tiles: List<Tile>) : Settlement(tiles) {

    constructor(settlement: Settlement) : this(settlement.tiles)


    override fun calculateOutput(): Map<Resource, Double> {
        return super.calculateOutput().mapValues {
            it.value * 2
        }
    }
}