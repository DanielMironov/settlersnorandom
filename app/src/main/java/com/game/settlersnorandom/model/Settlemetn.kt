package com.game.settlersnorandom.model

/**
 * Created by Daniel Mironov on 22.03.2019
 */

open class Settlement(val tiles: List<Tile>) {


    init {
        if (tiles.size != 3) {
            throw IllegalArgumentException("Settlement must have 3 tiles around it!")
        }
    }

    open fun calculateOutput(): Map<Resource, Double> {
        val result = emptyMap<Resource, Double>().toMutableMap()
        tiles.forEach { result[it.resource] = it.calculateResourceAmount() }
        return result.toMap()
    }

    fun sameAs(other: Settlement): Boolean {
        return calculateOutput() == other.calculateOutput()
    }
}