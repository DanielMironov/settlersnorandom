package com.game.settlersnorandom.model

/**
 * Created by Daniel Mironov on 23.03.2019
 */

enum class PlayerColor {
    RED, BLACK, WHITE, YELLOW
}