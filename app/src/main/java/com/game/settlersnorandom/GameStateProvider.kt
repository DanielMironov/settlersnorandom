package com.game.settlersnorandom

import com.game.settlersnorandom.model.GameStateImpl
import com.game.settlersnorandom.ui.GameState

object GameStateProvider {
    private val gameState: GameState = GameStateImpl()

    fun provide(): GameState {
        return gameState
    }
}