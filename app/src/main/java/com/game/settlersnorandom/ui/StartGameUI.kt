package com.game.settlersnorandom.ui

import com.game.settlersnorandom.model.PlayerColor
import com.game.settlersnorandom.model.Settlement

/**
 * Created by Daniel Mironov on 05.04.2019
 */

interface StartGameUI : GameUI {

    fun startGame(settlementsPerPlayer: Map<PlayerColor, Set<Settlement>>) {
        gameState.start(settlementsPerPlayer)
    }
}