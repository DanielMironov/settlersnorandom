package com.game.settlersnorandom.ui

import com.game.settlersnorandom.view.SettlementView
import com.game.settlersnorandom.model.PlayerColor

interface PlayerGameUI : GameUI {
    fun showState(playerColor: PlayerColor)

    fun upgradeSettlement(settlement: SettlementView, playerColor: PlayerColor) {
        gameState.upgradeSettlement(settlement, playerColor)
    }
}