package com.game.settlersnorandom.ui

import com.game.settlersnorandom.model.PlayerColor
import com.game.settlersnorandom.model.Resource

interface TurnGameUI : GameUI {
    fun nextTurn() {
        val incomePerPlayer = gameState.nextTurn()
        showIncome(incomePerPlayer)
    }

    fun showIncome(incomePerPlayer: Map<PlayerColor, Map<Resource, Int>>)
}