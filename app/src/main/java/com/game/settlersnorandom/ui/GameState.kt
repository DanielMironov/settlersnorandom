package com.game.settlersnorandom.ui

import com.game.settlersnorandom.model.PlayerColor
import com.game.settlersnorandom.model.Resource
import com.game.settlersnorandom.model.Settlement
import com.game.settlersnorandom.view.PlayerView
import com.game.settlersnorandom.view.SettlementView

/**
 * Created by Daniel Mironov on 05.04.2019
 */

interface GameState {

    fun getPlayersViews(): List<PlayerView>

    fun start(settlementsPerPlayer: Map<PlayerColor, Set<Settlement>>)

    fun nextTurn(): Map<PlayerColor, Map<Resource, Int>>

    fun addSettlement(settlement: Settlement, playerColor: PlayerColor)

    fun upgradeSettlement(settlement: SettlementView, playerColor: PlayerColor)
}