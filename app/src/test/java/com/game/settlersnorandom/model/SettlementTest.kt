package com.game.settlers.model

import com.game.settlersnorandom.model.Resource
import com.game.settlersnorandom.model.Settlement
import com.game.settlersnorandom.model.Tile
import com.game.settlersnorandom.model.Town
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Daniel Mironov on 26.03.2019
 */

class SettlementTest {

    @Test
    fun testOutputCalculate() {
        val tile1 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 1.0
            on { resource } doReturn Resource.WOOD
        }
        val tile2 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 2.0
            on { resource } doReturn Resource.WHEAT
        }
        val tile3 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 3.0
            on { resource } doReturn Resource.STONE
        }
        val settlement = Settlement(listOf(tile1, tile2, tile3))
        val expectedOutput: Map<Resource, Double> = mapOf(
            Pair(Resource.WOOD, 1.0),
            Pair(Resource.WHEAT, 2.0),
            Pair(Resource.STONE, 3.0)
        )
        assertEquals(expectedOutput, settlement.calculateOutput())
    }

    @Test
    fun testTownOutputCalculate() {
        val tile1 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 1.0
            on { resource } doReturn Resource.WOOD
        }
        val tile2 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 2.0
            on { resource } doReturn Resource.WHEAT
        }
        val tile3 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 3.0
            on { resource } doReturn Resource.STONE
        }
        val settlement = Town(listOf(tile1, tile2, tile3))
        val expectedOutput: Map<Resource, Double> = mapOf(
            Pair(Resource.WOOD, 2.0),
            Pair(Resource.WHEAT, 4.0),
            Pair(Resource.STONE, 6.0)
        )
        assertEquals(expectedOutput, settlement.calculateOutput())
    }

    @Test(expected = IllegalArgumentException::class)
    fun testIllegalTileCount() {
        val tile1 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 1.0
            on { resource } doReturn Resource.WOOD
        }
        val tile2 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 2.0
            on { resource } doReturn Resource.WHEAT
        }
        val tile3 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 3.0
            on { resource } doReturn Resource.STONE
        }
        val tile4 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 3.0
            on { resource } doReturn Resource.CLAY
        }
        Settlement(listOf(tile1, tile2, tile3, tile4))
    }
}