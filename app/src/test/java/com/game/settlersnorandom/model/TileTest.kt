package com.game.settlers.model

import com.game.settlersnorandom.model.Resource
import com.game.settlersnorandom.model.Tile
import com.game.settlersnorandom.model.incomeCoefficient
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Daniel Mironov on 22.03.2019
 */

class TileTest {
    private var tile: Tile = Tile(Resource.NONE, 2)

    @Test
    fun testChanceCalculating() {
        assertEquals(1 / 36.0 * incomeCoefficient, tile.calculateResourceAmount(), 0.01)
        tile = Tile(Resource.WHEAT, 6)
        assertEquals(5 / 36.0 * incomeCoefficient, tile.calculateResourceAmount(), 0.01)
        tile = Tile(Resource.CLAY, 8)
        assertEquals(5 / 36.0 * incomeCoefficient, tile.calculateResourceAmount(), 0.01)
        tile = Tile(Resource.WOOD, 12)
        assertEquals(1 / 36.0 * incomeCoefficient, tile.calculateResourceAmount(), 0.01)
    }

    @Test(expected = IllegalArgumentException::class)
    fun testIllegalDigit() {
        tile = Tile(Resource.STONE, 40)
    }
}