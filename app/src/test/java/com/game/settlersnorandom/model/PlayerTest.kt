package com.game.settlers.model

import com.game.settlersnorandom.model.*
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Created by Daniel Mironov on 30.03.2019
 */

class PlayerTest {

    @Test
    fun testIncomeCalculating() {
        val player = getTestPlayer()
        val expected = mapOf(
            Pair(Resource.CLAY, 2),
            Pair(Resource.WOOD, 3),
            Pair(Resource.WHEAT, 4),
            Pair(Resource.STONE, 5)
        )
        assertEquals(expected, player.calculateIncome())
    }

    @Test
    fun testAddingSettlement() {
        val player = getTestPlayer()
        val settlement = mock<Settlement> {
            on { calculateOutput() } doReturn mapOf(Pair(Resource.CLAY, 3.2), Pair(Resource.WOOD, 4.7))
        }
        player.addSettlement(settlement)
        val expected = mapOf(
            Pair(Resource.CLAY, 5),
            Pair(Resource.WOOD, 8),
            Pair(Resource.WHEAT, 4),
            Pair(Resource.STONE, 5)
        )
        assertEquals(expected, player.calculateIncome())
    }

    @Test
    fun testSettlementUpgrading() {
        val player = getTestPlayer()
        val tile1 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 1.3
            on { resource } doReturn Resource.WOOD
        }
        val tile2 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 2.5
            on { resource } doReturn Resource.WHEAT
        }
        val tile3 = mock<Tile> {
            on { calculateResourceAmount() } doReturn 3.3
            on { resource } doReturn Resource.STONE
        }
        val settlement = Settlement(listOf(tile1, tile2, tile3))
        player.addSettlement(settlement)
        player.upgradeSettlement(settlement)
        val expected = mapOf(
            Pair(Resource.CLAY, 2),
            Pair(Resource.WOOD, 5),
            Pair(Resource.WHEAT, 9),
            Pair(Resource.STONE, 12)
        )
        assertEquals(expected, player.calculateIncome())
    }

    private fun getTestPlayer(): Player {
        val settlement1 = mock<Settlement> {
            on { calculateOutput() } doReturn mapOf(Pair(Resource.CLAY, 2.2), Pair(Resource.WOOD, 3.3))
        }
        val settlement2 = mock<Settlement> {
            on { calculateOutput() } doReturn mapOf(Pair(Resource.WHEAT, 4.4), Pair(Resource.STONE, 5.5))
        }
        val player = Player(PlayerColor.RED)
        player.start(settlement1, settlement2)
        return player
    }
}